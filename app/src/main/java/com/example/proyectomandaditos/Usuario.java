package com.example.proyectomandaditos;
// Clase Usuario para el registro, login y editarPerfil
public class Usuario {
    public String usuario;
    public String nombre;
    public String apellidos;
    public String calle;
    public String numCasa;
    public String colonia;
    public String referencia;
    public String telefono;
    public String correo;
    public String password;
    public String confirmarPassword;

    public Usuario(String usuario, String nombre, String apellidos, String calle, String numCasa, String colonia, String referencia, String telefono, String correo, String password, String confirmarPassword) {
        this.usuario = usuario;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.calle = calle;
        this.numCasa = numCasa;
        this.colonia = colonia;
        this.referencia = referencia;
        this.telefono = telefono;
        this.correo = correo;
        this.password = password;
        this.confirmarPassword = confirmarPassword;
    }

    public Usuario(String nombre, String apellidos, String calle, String colonia, String telefono, String correo, String password) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.calle = calle;
        this.colonia = colonia;
        this.telefono = telefono;
        this.correo = correo;
        this.password = password;
    }
}

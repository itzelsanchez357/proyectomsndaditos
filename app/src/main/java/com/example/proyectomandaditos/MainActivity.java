package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnLonginA = (Button) findViewById(R.id.btnLogin);
        Button btnRegistroA = (Button) findViewById(R.id.btnRegistro);

        btnLonginA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),LoginActivity.class);
                startActivity(i);
            }
        });
        btnRegistroA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),RegistroActivity.class);
                startActivity(i);
            }
        });
    }
}
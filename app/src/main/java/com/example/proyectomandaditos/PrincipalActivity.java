package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class PrincipalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        // Casteo de botones flotantes
        FloatingActionButton btnInicio = (FloatingActionButton) findViewById(R.id.fBtnInicio);
        FloatingActionButton btnPerfil = (FloatingActionButton) findViewById(R.id.fBtnPerfil);
        FloatingActionButton btnSeguimiento = (FloatingActionButton) findViewById(R.id.fBtnSeguimiento);
        FloatingActionButton btnPedido = (FloatingActionButton) findViewById(R.id.fBtnPedido);

        // Evento  a boton
        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Se crea intent donde se le asigna el nombre i que llevara a PrincipalActivity y despues inicia
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                startActivity(i);
            }
        });
        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se obtienen valores del putExtra que fueron enviados del activity Login
                String nombre_extra = getIntent().getExtras().getString("nombre_registro_extra_l");
                String password_extra = getIntent().getExtras().getString("password_registro_extra_l");
                String apellidos_extra = getIntent().getExtras().getString("apellidos_registro_extra_l");
                String calle_extra = getIntent().getExtras().getString("calle_registro_extra_l");
                String colonia_extra = getIntent().getExtras().getString("colonia_registro_extra_l");
                String telefono_extra = getIntent().getExtras().getString("telefono_registro_extra_l");
                String correo_extra = getIntent().getExtras().getString("correo_registro_extra_l");

                // Se crea intent donde se le asigna el nombre i que llevara a Miperfilctivity
                Intent i = new Intent(v.getContext(),MiPerfilActivity.class);
                // Se va a mandar al actiity MiPerfil los siguientes valores con un name asignado
                i.putExtra("nombre_registro_extra_prin", nombre_extra);
                i.putExtra("password_registro_extra_prin", password_extra);
                i.putExtra("apellidos_registro_extra_prin", apellidos_extra);
                i.putExtra("calle_registro_extra_prin", calle_extra);
                i.putExtra("colonia_registro_extra_prin", colonia_extra);
                i.putExtra("telefono_registro_extra_prin", telefono_extra);
                i.putExtra("correo_registro_extra_prin", correo_extra);
                startActivity(i);
            }
        });
        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se crea intent donde se le asigna el nombre i que llevara a PedidosActivity
                Intent i = new Intent(v.getContext(),PedidosActivity.class);
                startActivity(i);
            }
        });
        btnSeguimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se crea intent donde se le asigna el nombre i que llevara a SeguimientoActivity
                Intent i = new Intent(v.getContext(),SeguimientoActivity.class);
                startActivity(i);
            }
        });
    }
}
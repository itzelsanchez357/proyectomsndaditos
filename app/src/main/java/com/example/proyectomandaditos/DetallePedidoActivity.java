package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DetallePedidoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_pedido);
//casteo de botones
        Button btnGguardarDetallePedido = (Button) findViewById(R.id.btnGguardarDetallePedido);

        //variables obtenidas del activity_pedido
        String nombre_extra = getIntent().getExtras().getString("nombre_registro_extra");
        String descripcion_extra = getIntent().getExtras().getString("descripcion_registro_extra");
        String precio_extra = getIntent().getExtras().getString("precio_registro_extra");


        //Se crea las variables etiquetas
        TextView etiqueta,etiqueta2,etiqueta3,etiqueta4,etiqueta5;

        //Se identifica y se asigna el nuevo texto a la etiqueta TextView
        etiqueta=(TextView) findViewById(R.id.txtn);
        etiqueta.setText(nombre_extra);
        etiqueta2=(TextView) findViewById(R.id.txtd);
        etiqueta2.setText(descripcion_extra);
        etiqueta3=(TextView) findViewById(R.id.txtp);
        etiqueta3.setText(precio_extra);
        
        etiqueta5=(TextView) findViewById(R.id.textTotal2);
        etiqueta5.setText(precio_extra);

        //Evento del boton
        btnGguardarDetallePedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Imprime mensaje alert en activityDetallePedido
                Toast.makeText(DetallePedidoActivity.this, "Tu pedido fue enviado con exito", Toast.LENGTH_SHORT).show();
                //Intent para cambiar a activity principal
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                //inicia activity
                startActivity(i);
            }
        });
    }
}

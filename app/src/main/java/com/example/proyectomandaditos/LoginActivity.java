package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Casteo de botones y edit text
        Button btnLogin = (Button) findViewById(R.id.btnLoginIniciar);
        EditText edtUsuarioIniciar = (EditText) findViewById(R.id.edtUsuarioIniciar);
        EditText edtPasswordIniciar = (EditText) findViewById(R.id.edtPasswordIniciar);

        // Se obtienen valores del putExtra que fueron enviados del activity Registro
        String usuario_extra = getIntent().getExtras().getString("usuario_registro_extra");
        String password_extra = getIntent().getExtras().getString("password_registro_extra");
        String nombre_extra = getIntent().getExtras().getString("nombre_registro_extra");
        String apellidos_extra = getIntent().getExtras().getString("apellidos_registro_extra");
        String calle_extra = getIntent().getExtras().getString("calle_registro_extra");
        String colonia_extra = getIntent().getExtras().getString("colonia_registro_extra");
        String telefono_extra = getIntent().getExtras().getString("telefono_registro_extra");
        String correo_extra = getIntent().getExtras().getString("correo_registro_extra");

        // Evento al boton, se hace una acción al dar click en el mismo
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Condicion; Si el campo de usuario o password están vacios arroja un mensaje de error
                if(edtUsuarioIniciar.getText().toString().isEmpty() || edtPasswordIniciar.getText().toString().isEmpty()){
                    Toast.makeText(LoginActivity.this,  "¡Error! Usuario o Password Vacio", Toast.LENGTH_SHORT).show();
                }else if(edtUsuarioIniciar.getText().toString().equals(usuario_extra) && edtPasswordIniciar.getText().toString().equals(password_extra)){
                    // Si el campo de usuario y password es igual al que se registro llevara a la siguiente pantalla

                    Intent i = new Intent(v.getContext(), PrincipalActivity.class);

                    // Se manda a PrincipalActivity los valores de nombre, apellidos, calle... con su respectivo name
                    i.putExtra("nombre_registro_extra_l", nombre_extra);
                    i.putExtra("password_registro_extra_l", password_extra);
                    i.putExtra("apellidos_registro_extra_l", apellidos_extra);
                    i.putExtra("calle_registro_extra_l", calle_extra);
                    i.putExtra("colonia_registro_extra_l", colonia_extra);
                    i.putExtra("telefono_registro_extra_l", telefono_extra);
                    i.putExtra("correo_registro_extra_l", correo_extra);
                    startActivity(i);
                }else{
                    // De lo contrario mostrara un error que el usuario p password es incorrecto o no corresponde con el registro
                    Toast.makeText(LoginActivity.this,  "¡Error! Usuario o Password incorrecto", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MiPerfilActivity extends AppCompatActivity {
    // Creación de usuario
    Usuario u;

    // Asignar un nombre para castear
    EditText txt_texto;
    EditText txt_apellidoEditar;
    EditText txt_CalleEditar;
    EditText txt_ColoniaEditar;
    EditText txt_tel;
    EditText txt_Correo;
    EditText txt_contrasena;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_perfil);

        // Casteo de botones respecto a su id
        Button btnGuardarPerfil = (Button) findViewById(R.id.btnGuardarPerfil);
        Button btnEditarFoto = (Button) findViewById(R.id.btn_edit_img2);
        Button btnEditarNombre = (Button) findViewById(R.id.btn_edit_nom);
        Button btnEditarApellidos = (Button) findViewById(R.id.btn_edit_apellido);
        Button btnEditarTelefono = (Button) findViewById(R.id.btn_edit_tel);
        Button btnEditarCorreo = (Button) findViewById(R.id.btn_edit_correo);
        Button btnEditarContra = (Button) findViewById(R.id.btn_edit_contra);
        Button btn_edit_direccion = (Button) findViewById(R.id.btn_edit_direccion);

        // casteo de botones flotantes respecto a su id
        FloatingActionButton btnInicio = (FloatingActionButton) findViewById(R.id.fBtnInicioM);
        FloatingActionButton btnPerfil = (FloatingActionButton) findViewById(R.id.fBtnPerfilM);
        FloatingActionButton btnSeguimiento = (FloatingActionButton) findViewById(R.id.fBtnSeguimientoM);
        FloatingActionButton btnPedido = (FloatingActionButton) findViewById(R.id.fBtnPedidoM);

        // Casteo del editText para nombre respecto a su id
        txt_texto = (EditText) findViewById(R.id.txt_nombre);
        // Se obtiene el valor mandado del activity principal y lo asigna al EditText
        String nombre_extra = getIntent().getExtras().getString("nombre_registro_extra_prin");
        txt_texto.setText(nombre_extra);

        // Casteo del editText  para apellidos respecto a su id
        txt_apellidoEditar = (EditText) findViewById(R.id.txt_apellidoEditar);
        // Se obtiene el valor mandado del activity principal y lo asigna al EditText
        String apellidos_extra = getIntent().getExtras().getString("apellidos_registro_extra_prin");
        txt_apellidoEditar.setText(apellidos_extra);

        // Casteo del editText para calle respecto a su id
        txt_CalleEditar = (EditText) findViewById(R.id.txt_CalleEditar);
        // Se obtiene el valor mandado del activity principal y lo asigna al EditText
        String calle_extra = getIntent().getExtras().getString("calle_registro_extra_prin");
        txt_CalleEditar.setText(calle_extra);

        // Casteo del editText para colonia respecto a su id
        txt_ColoniaEditar = (EditText) findViewById(R.id.txt_ColoniaEditar);
        // Se obtiene el valor mandado del activity principal y lo asigna al EditText
        String colonia_extra = getIntent().getExtras().getString("colonia_registro_extra_prin");
        txt_ColoniaEditar.setText(colonia_extra);

        // Casteo del editText para telefono respecto a su id
        txt_tel = (EditText) findViewById(R.id.txt_tel);
        // Se obtiene el valor mandado del activity principal y lo asigna al EditText
        String telefono_extra = getIntent().getExtras().getString("telefono_registro_extra_prin");
        txt_tel.setText(telefono_extra);

        // Casteo del editText para correo respecto a su id
        txt_Correo = (EditText) findViewById(R.id.txt_Correo);
        // Se obtiene el valor mandado del activity principal y lo asigna al EditText
        String correo_extra = getIntent().getExtras().getString("correo_registro_extra_prin");
        txt_Correo.setText(correo_extra);

        // Casteo del editText para contraseña respecto a su id
        txt_contrasena = (EditText) findViewById(R.id.txt_contrasena);
        // Se obtiene el valor mandado del activity principal y lo asigna al EditText
        String password_extra = getIntent().getExtras().getString("password_registro_extra_prin");
        txt_contrasena.setText(password_extra);


        btnInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se obtiene el valor de los editText y se guardan en variables
                String nombre = txt_texto.getText().toString();
                String apellidos = txt_apellidoEditar.getText().toString();
                String calle = txt_CalleEditar.getText().toString();
                String colonia = txt_ColoniaEditar.getText().toString();
                String telefono = txt_tel.getText().toString();
                String correo = txt_Correo.getText().toString();
                String password = txt_contrasena.getText().toString();

                // Instancia de Usuario con los valores obtenidos en la variables
                u = new Usuario(nombre,apellidos,calle,colonia,telefono,correo,password);
                //Se crea intent donde se le asigna el nombre i que llevara a PrincipalActivity y despues inicia
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                startActivity(i);
            }
        });

        // Si se da click en algunos de los botones siguintes los botones se vuelven Enables o editables
        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),MiPerfilActivity.class);
                startActivity(i);
            }
        });
        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PedidosActivity.class);
                startActivity(i);
            }
        });
        btnSeguimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),SeguimientoActivity.class);
                startActivity(i);
            }
        });


        btnGuardarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(),PrincipalActivity.class);
                startActivity(i);
            }
        });
        btnEditarNombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            txt_texto.setEnabled(true);
            }
        });
        btnEditarApellidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            txt_apellidoEditar.setEnabled(true);
            }
        });
        btnEditarTelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_tel.setEnabled(true);
            }
        });
        btnEditarCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_Correo.setEnabled(true);
            }
        });
        btnEditarContra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_contrasena.setEnabled(true);
            }
        });
        btn_edit_direccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_CalleEditar.setEnabled(true);
                txt_ColoniaEditar.setEnabled(true);
            }
        });
    }
}
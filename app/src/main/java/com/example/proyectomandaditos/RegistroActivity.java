package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistroActivity extends AppCompatActivity {

    // Se crea Instancia nombrado u de la clase Usuario
    Usuario u;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        // Casteo del Boton para registro
        Button btnAceptarRegistro =  (Button) findViewById(R.id.btnAceptarRegistro);
        // Se le asigna metodo al boton
        btnAceptarRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Casteo de los EditText a partir de su id
                EditText edtUsuarioRegistro = (EditText) findViewById(R.id.edtUsuarioRegistro);
                EditText edtNombreRegistro = (EditText) findViewById(R.id.edtNombreRegistro);
                EditText edtApellidosRegistro = (EditText) findViewById(R.id.edtApellidosRegistro);
                EditText edtCalleRegistro = (EditText) findViewById(R.id.edtCalleRegistro);
                EditText edtNumCasaRegistro = (EditText) findViewById(R.id.edtNumCasaRegistro);
                EditText edtColoniaRegistro = (EditText) findViewById(R.id.edtColoniaRegistro);
                EditText edtRefReg = (EditText) findViewById(R.id.edtRefReg);
                EditText edtTelefonoRegistro = (EditText) findViewById(R.id.edtTelefonoRegistro);
                EditText edtCorreoRegistro = (EditText) findViewById(R.id.edtCorreoRegistro);
                EditText edtPasswordRegistro = (EditText) findViewById(R.id.edtPasswordRegistro);
                EditText edtPasswordConfirmarRegistro = (EditText) findViewById(R.id.edtPasswordConfirmarRegistro);

                // Se obtiene el valor de los EditText y se guardan en una variable
                String usuario = edtUsuarioRegistro.getText().toString();
                String nombre = edtNombreRegistro.getText().toString();
                String apellidos = edtApellidosRegistro.getText().toString();
                String calle = edtCalleRegistro.getText().toString();
                String numCasa = edtNumCasaRegistro.getText().toString();
                String colonia = edtColoniaRegistro.getText().toString();
                String referencia = edtRefReg.getText().toString();
                String telefono = edtTelefonoRegistro.getText().toString();
                String correo = edtCorreoRegistro.getText().toString();
                String password = edtPasswordRegistro.getText().toString();
                String confirmarPassword = edtPasswordConfirmarRegistro.getText().toString();

                //n.registrar(usuario,nombre,apellidos,calle,numCasa,colonia,referencia,telefono,correo,password,confirmarPassword);

                // Si password es diferente de Confirmar Password arroja un error
                if(!password.equals(confirmarPassword)){
                    Toast.makeText(RegistroActivity.this, "¡Error! No coincide la contraseña", Toast.LENGTH_SHORT).show();
                }else if(usuario.isEmpty()||nombre.isEmpty()||apellidos.isEmpty()||calle.isEmpty()||numCasa.isEmpty()||colonia.isEmpty()||referencia.isEmpty()||telefono.isEmpty()||correo.isEmpty()||password.isEmpty()||confirmarPassword.isEmpty()){
                    Toast.makeText(RegistroActivity.this, "¡Error! Algún campo esta vacío", Toast.LENGTH_SHORT).show();
                }else{
                    // Se le dan sus respectivos valores de la instancia de Usuario que se hizo previamente
                    u = new Usuario(usuario, nombre, apellidos, calle, numCasa, colonia, referencia, telefono, correo, password, confirmarPassword);
                    // Se crea intent donde se le asigna el nombre i que llevara a LoginActivity
                    Intent i = new Intent(v.getContext(), LoginActivity.class);
                    // Al momento de llevar a LoginActivity se le pasa el valor del usuario y password con un name
                    i.putExtra("usuario_registro_extra", u.usuario);
                    i.putExtra("nombre_registro_extra", u.nombre);
                    i.putExtra("apellidos_registro_extra", u.apellidos);
                    i.putExtra("calle_registro_extra", u.calle);
                    i.putExtra("colonia_registro_extra", u.colonia);
                    i.putExtra("telefono_registro_extra", u.telefono);
                    i.putExtra("correo_registro_extra", u.correo);
                    i.putExtra("password_registro_extra", u.password);

                    // Inicia o se muestra el activity
                    startActivity(i);
                }
            }
        });
    }
}
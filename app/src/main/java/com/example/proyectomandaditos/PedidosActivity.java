package com.example.proyectomandaditos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class PedidosActivity extends AppCompatActivity {
    //clase pedidos
    Pedidos p;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);

        Button btnAceptarPedido =(Button) findViewById(R.id.btnAceptarPedido);

        btnAceptarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //se castea los EditText con sus id
                EditText edtNombrePedido = (EditText) findViewById(R.id.edtNombrePedido);
                EditText edtDescripcionPedido = (EditText) findViewById(R.id.edtDescripcionPedido);
                EditText edtPrecioPedido = (EditText) findViewById(R.id.edtPrecioPedido);

                //variables con nombres de las etiquetas
                String nombre = edtNombrePedido.getText().toString();
                String descripcion = edtDescripcionPedido.getText().toString();
                String precio = edtPrecioPedido.getText().toString();


                //clase pedidos las variables
                p=new Pedidos(nombre, descripcion,precio);

                //Intent para cambiar a activity detalle pedido
                Intent i = new Intent(v.getContext(),DetallePedidoActivity.class);
                //Se le envia al activity y se le asigna un name para reconocer que es lo que se envia
                i.putExtra( "nombre_registro_extra",p.nombre);
                i.putExtra( "descripcion_registro_extra",p.descripcion);
                i.putExtra( "precio_registro_extra",p.precio);
                //Inicia activity
                startActivity(i);
            }
        });
    }
}
